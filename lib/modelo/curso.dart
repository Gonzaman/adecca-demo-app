class Curso{
  final int id;

  final String nombre;
  final String coordinador;

  final int numActividades;
  final int numFechasProx;
  final int numAvisos;

  Curso(this.id, this.numActividades, this.numFechasProx, this.numAvisos, {required this.nombre, required this.coordinador});
}


class DetalleCurso {
  final int id;

  final String nombre;
  final String coordinador;

  final String descripcion; // Probablemente despues lo cambiemos
  final String portada;

  DetalleCurso({required this.id, required this.nombre, required this.coordinador, required this.descripcion, required this.portada});

}


class Aviso {
  // No hay un curso son avisos, asi que por ahora lo dejamos así
  final String texto;

  Aviso({required this.texto});
}