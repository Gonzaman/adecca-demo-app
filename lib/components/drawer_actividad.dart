import 'package:adecca_app/components/drawer.dart';
import 'package:adecca_app/pages/lista_cursos.dart';
import 'package:adecca_app/pages/login.dart';
import 'package:flutter/material.dart';

class CursoDrawer extends StatelessWidget {
  const CursoDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicDrawer(
      extras: <Widget>[
          ListTile(
            leading: Icon(Icons.bug_report),
            title: Text("Login"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => Login()));
            },
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text("Cursos"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListaCursos()));
            },
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text("Portada del curso"),
            onTap: () {
              Navigator.of(context).popAndPushNamed("/portada");
            },
          ),
          ListTile(
            leading: Icon(Icons.calendar_month),
            title: Text("Calendario"),
            onTap: () {
              Navigator.of(context).pushNamed("/calendario");
            },
          ),
          // Actvidades Start
          const ExpansionTile(
            title: Text("Laboratorios"),
            leading: Icon(Icons.folder),
            children: [
              ListTile(
                title: Text("Laboratorio 1 - Entrevista"),
                leading: Icon(Icons.person),
              ),
              ListTile(
                title: Text("Laboratorio 2 - BPMN"),
                leading: Icon(Icons.person),
              )
            ],
          ),
          const ExpansionTile(
            title: Text("Proyecto"),
            leading: Icon(Icons.folder),
            children: [
              ListTile(
                title: Text("Inscripción empresa"),
                leading: Icon(Icons.message),
              ),
              ListTile(
                title: Text("Informe 1"),
                leading: Icon(Icons.person),
              )
            ],
          ),
          // Actvidades End
          ListTile(
            leading: Icon(Icons.video_camera_front),
            title: Text("Videochat"),
            onTap: () {
              Navigator.of(context).pushReplacementNamed("/videochat");
            },
          ),
          ListTile(
            leading: Icon(Icons.list),
            title: Text("Calificaciones"),
            onTap: () {
              Navigator.of(context).pushReplacementNamed("/calificaciones");
            },
          ),
        ]
    );
  }
}