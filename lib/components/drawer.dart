import 'package:adecca_app/pages/settings.dart';
import 'package:flutter/material.dart';

class BasicDrawer extends StatelessWidget {
  final List<Widget> top = const <Widget> [
    DrawerHeader(child: ListTile(
      title: Text("Gonzalo Inostroza"),
      leading: Icon(Icons.person),
    )),
  ];

  final List<Widget> extras;

  const BasicDrawer({super.key, this.extras = const []});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: top + extras + [
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text("Ajustes"),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SettingsPage()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text("Cerrar Sesión"),
            onTap: () {
              Navigator.of(context).pushNamed("/login");
            },
          ),
        ],
      ),
    );
  }
}