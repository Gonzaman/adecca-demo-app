import 'package:adecca_app/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:adecca_app/pages/activadad_pages.dart';

void main() => runApp(const AdeccaApp());

class AdeccaApp extends StatelessWidget {
  const AdeccaApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        "/login": (BuildContext context) => const LoginForm(),
        "/portada": (BuildContext context) => const PortadaPage(),
        "/calendario": (BuildContext context) => const CalendarioPage(),
        "/videochat": (BuildContext context) => const VideochatPage(),
        "/calificaciones": (BuildContext context) => const CalificacionesPage(),
      },
      initialRoute: "/login",
      title: "Adecca",
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: const ColorScheme.light(),
      ),
      darkTheme: ThemeData(
        useMaterial3: true,
        colorScheme: const ColorScheme.dark(),
      ),
      themeMode: ThemeMode.system,
    );
  }
}