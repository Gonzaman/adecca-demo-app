import 'package:adecca_app/components/drawer.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext) {
    return Scaffold(
      appBar: AppBar(title: const Text("Adecca"),),
      drawer: const BasicDrawer(),
    );
  }
}