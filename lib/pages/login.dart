import 'package:flutter/material.dart';

import 'lista_cursos.dart';

// Rut y contraseña validos:
// 11.111.111-1
// 1234

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Adecca"),
      ),
      body: const LoginForm(),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<LoginFormState>();

  final sampleRut = '11.111.111-1';
  final samplePassword = '1234';

  final textRut = TextEditingController();
  final textPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Ingresar Rut',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Porfavor, ingresar un rut';
                }
                return null;
              },
              controller: textRut,
            ),
            TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Ingresar Contraseña',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Porfavor, ingresar un rut';
                }
                return null;
              },
              controller: textPassword,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ElevatedButton(
                onPressed: () {
                  if (validate()) {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListaCursos()));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                          content:
                          Text('Rut o contraseña ingresada no es valida')),
                    );
                  }
                },
                child: const Text('Submit'),
              ),
            )
          ],
        ),
      )
    );
  }

  bool validate() {
    return (textRut.text == sampleRut && textPassword.text == samplePassword);
  }
}
