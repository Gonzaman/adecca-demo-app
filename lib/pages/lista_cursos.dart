import 'package:adecca_app/components/drawer.dart';
import 'package:flutter/material.dart';
import 'package:adecca_app/modelo/curso.dart';

class ListaCursos extends StatefulWidget{
  @override
  ListaCursosState createState() => ListaCursosState();
}

class ListaCursosState extends State<ListaCursos> {
  @override
  Widget build(BuildContext context) {
    List<Curso> cursos = [];
    return Scaffold(
      appBar: AppBar(
        title: Text("Cursos"),
      ),
      drawer: BasicDrawer(),
      body: ListView(
        children: cursos.map((curso){
          return ListTile(
            title: Text(curso.nombre),
            subtitle: Text("Coordinador: ${curso.coordinador}"),
            onTap: () {
              SnackBar snack = SnackBar(content: Text("Presionaste: ${curso.nombre}"));
              ScaffoldMessenger.of(context).showSnackBar(snack);
            },
          );
        }).toList(),
      ),
    );
  }
}