import 'package:flutter/material.dart';
import 'package:adecca_app/components/drawer_actividad.dart';


class PortadaPage extends StatelessWidget {
  const PortadaPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          drawer: const CursoDrawer(),
          appBar: AppBar(
            title: const Text("Modelamiento de procesos"),
            bottom: const TabBar(
              tabs: [
                Tab(text: "Portada",),
                Tab(text: "Avisos",),
                Tab(text: "Participantes",),
              ],
            ),
          ),
          body: const TabBarView(
            children: <Widget>[
              Center(child: Text("Descripción del Curso")),
              Center(child: Text("No hay avisos que mostrar")),
              Center(child: Text("Listado de personas")),
            ],
          ),
        )
    );
  }
}

class CalendarioPage extends StatelessWidget {
  const CalendarioPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CursoDrawer(),
      appBar: AppBar(title: const Text("Modelamiento de procesos"),),
      body: const Center(child: Text("Calendario"),),
    );
  }
}

class VideochatPage extends StatelessWidget {
  const VideochatPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CursoDrawer(),
      appBar: AppBar(title: const Text("Modelamiento de procesos"),),
      body: const Center(child: Text("Zoom link"),),
    );
  }
}

class CalificacionesPage extends StatelessWidget {
  const CalificacionesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CursoDrawer(),
      appBar: AppBar(title: const Text("Modelamiento de procesos"),),
      body: const Center(child: Text("Me lo eché"),),
    );
  }
}