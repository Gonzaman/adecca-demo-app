import '../modelo/curso.dart';

abstract class CursoRepo {
  Future<List<Curso>> getCursos();

  Future<DetalleCurso> getCursoById(int id);

  Future<int> getNumAvisosById(int cursoId);

  Future<List<Aviso>> getAvisosById(int cursoId);
}


/*
class BasicCursoRepo implements CursoRepo {
  final List<Curso> _cursos = [
    Curso(nombre: "Modelamiento", coordinador: "Luis Rojas", id: 1),
    Curso(nombre: "Estructuras de Datos", coordinador: "Gilberto Gutierrez", id: 2),
  ];

  @override
  Curso getCursoById(int id) {
    for (Curso curso in _cursos) {
      if (curso.id == id) {
        return curso;
      }
    }
    throw Error();
  }

  @override
  List<Curso> getCursos() {
    return _cursos;
  }
}
 */