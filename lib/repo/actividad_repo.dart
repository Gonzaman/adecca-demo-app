import '../modelo/actividad.dart';

abstract class ActividadRepo {
  Future<List<Actividad>> getActividadTree(int cursoId);

  Future<DetalleActividad> getActividadDetalle(int actividadId);
}