import '../modelo/session.dart';

abstract class SessionRepo {
  Future<bool> isLogIn();

  Future<void> logOut();

  Future<SessionInfo> getSessionInfo();

  Future<bool> logIn(String rut, String password);
}